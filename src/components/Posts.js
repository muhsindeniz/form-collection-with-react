import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { render } from '@testing-library/react';

let Posts = () => {

    let [sonuc, setSonuc] = useState();

    const { register, handleSubmit, errors } = useForm();

    const onSubmit = data => {
        const a = Number(data.nm1);
        const b = Number(data.nm2);

        setSonuc(a + b)

    }

    return (
        <div className="calculator">
            <h1>Toplama İşlemi</h1>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="lbl">Sayı 1</div>
                <input type="number" name="nm1" ref={register({ required: true })} />
                {errors.nm1 && <span>Lütfen bir değer giriniz!..</span>}

                <div className="lbl">Sayı 2</div>
                <input type="number" name="nm2" ref={register({ required: true })} />
                {errors.nm2 && <span>Lütfen bir değer giriniz!..</span>}

                <input type="submit" className="submitbtn" value="Hesapla" />

                <div className="snc">
                    Sonuc : {sonuc}
                </div>
            </form>
        </div>
    );
}

export default Posts;